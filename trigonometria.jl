#MAC0110 - MiniEP7
#Lorenzo Bertin - 11795356

#<funções da parte 1>
function sin(x)	
	i = 0
	indice = 1
	sinal = -1
	valor = 0
	while (i <= 10)		
		valor = BigFloat(valor + (-1 * sinal * BigFloat((BigFloat(x^BigInt(indice)))/factorial(BigInt(indice)))))
		sinal = sinal * -1
		indice = indice + 2
		i = i + 1
	end

	return(valor)
end

function cos(x)
	i = 0
	indice = 0
	sinal = -1
	valor = 0
	while (i <= 10)
		valor = BigFloat(valor + (-1)*sinal * ((x^BigInt(indice))/factorial(BigInt(indice))))
		sinal = sinal * -1
		indice = indice + 2
		i = i + 1
	end
	return valor
end

# Função necessaria para o cálculo da tangente
function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function tan(x)
	if (x > pi/2)
		x = x  -pi
		-tan(x)
	end
	i = 0
	n = 1 # mudança de nome na variável para que o algoritmo fique mais legível
	valor = 0
	while(i <= 10)
		numerador = BigFloat(2^(2*n) * (2^ (2*n) - 1) * BigFloat(bernoulli(n)) * x^(2*n-1))
		denominador = BigInt(factorial(BigInt(2*n)))
		valor = valor + BigFloat(numerador/denominador)
		n = n + 1
		i = i + 1
	end
	return valor

end

#<funções da parte2>
# Definindo função de aproximação para utilizar nos testes automatizados
function quaseigual(a,b)
	erro = 0.001
	if (abs(a-b) > erro)
		return false
	else
		return true
	end
end

function check_sin(value,x)
	if (quaseigual(value,sin(x)))
		return true
	else
		return false
	end
end

function check_cos(value,x)
	if (quaseigual(value,cos(x)))
		return true
	else
		return false
	end
end

function check_tan(value,x)
	if (quaseigual(value,tan(x)))
		return true
	else
		return false
	end
end



function taylor_sin(x)	
	i = 0
	indice = 1
	sinal = -1
	valor = 0
	while (i <= 10)		
		valor = BigFloat(valor + (-1 * sinal * BigFloat((BigFloat(x^BigInt(indice)))/factorial(BigInt(indice)))))
		sinal = sinal * -1
		indice = indice + 2
		i = i + 1
	end

	return(valor)
end

function taylor_cos(x)
	i = 0
	indice = 0
	sinal = -1
	valor = 0
	while (i <= 10)
		valor = BigFloat(valor + (-1)*sinal * ((x^BigInt(indice))/factorial(BigInt(indice))))
		sinal = sinal * -1
		indice = indice + 2
		i = i + 1
	end
	return valor
end

function taylor_tan(x)
	if (x > pi/2)
		x = x  -pi
		-tan(x)
	end
	i = 0
	n = 1 # mudança de nome na variável para que o algoritmo fique mais legível
	valor = 0
	while(i <= 10)
		numerador = BigFloat(2^(2*n) * (2^ (2*n) - 1) * BigFloat(bernoulli(n)) * x^(2*n-1))
		denominador = BigInt(factorial(BigInt(2*n)))
		valor = valor + BigFloat(numerador/denominador)
		n = n + 1
		i = i + 1
	end
	return valor

end

function test()
# Testes automatizados para o sin(x)
	if (!quaseigual(sin(pi/2),1))
		println("Erro com sin(pi/2)")
	end
	if (!quaseigual(sin(pi),0))
		println("Erro com sin(pi)")
	end
	if (!quaseigual(sin(0),0))
		println("Erro com sin(0)")
	end
	if (!quaseigual(sin(pi/3),0.8660))
		println("Erro com sin(pi/3)")
	end
	if (!quaseigual(sin(pi/6),0.5))
		println("Erro com sin(pi/6)")
	end
# Testes automatizados para o cos(x)
	if (!quaseigual(cos(pi/2),0))
		println("Erro com cos(pi/2)")
	end
	if (!quaseigual(cos(pi),-1))
		println("Erro com cos(pi)")
	end
	if (!quaseigual(cos(0),1))
		println("Erro com cos(0)" )
	end
	if (!quaseigual(cos(pi/3),0.5))
		println("Erro com cos(pi/3)")
	end
	if (!quaseigual(cos(pi/6),0.866))
		println("Erro com cos(pi/6)")
	end
# Testes automatizados para a tan(x)
	if (!quaseigual(tan(0),0))
		println("Erro com tan(0)")
	end	
	if (!quaseigual(tan(pi/3),1.73205))
		println("Erro com tan(pi/3)" )
	end
	if (!quaseigual(tan(pi/6),0.57735))
		println("Erro com tan(pi/6)")
	end
	if (!quaseigual(tan(pi/4),1))
		println("Erro com tan(pi/4)")
	end
# Testes automatizados para check_tan,check_sin,check_cos
	if(!check_sin(0.5, pi / 6))
		println("Erro com check_sin de pi/6")
	end
	if(!check_cos(0.5, pi / 3))
		println("Erro com check_cos de pi/3")
	end
	if(!check_tan(0,pi))
		println("Erro com check_tan de pi")
	end
	if(check_tan(0.8,pi/4))
		println("Erro com check_tan de pi/4")
	end
end
test()

